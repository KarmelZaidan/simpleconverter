package com.jawwal.fileconverters;

import org.apache.poi.ss.usermodel.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Properties;

public class ExcelToCSV {
	
	private Sheet sheet = null;
	private String csvpath = null;
	
	public ExcelToCSV(Sheet sheet, String csvpath) {
		this.sheet = sheet;
		this.csvpath = csvpath;
	}
	
	public ExcelToCSV() {
		
	}
	
	public OutputStream convertExcelToCSV(Sheet sheet, FormulaEvaluator formulaEveluater, Properties properties, String src) throws IOException {
		
		String generalpath = properties.getProperty("GENERAL_PATH");
		if(src.equals("WB")) {
			csvpath = properties.getProperty("WB_CSV_FILE_PATH");
        }else {
        	csvpath = properties.getProperty("G_CSV_FILE_PATH");
        }
		
		//Initialize and Create Directories
        String directorypath = "/"+generalpath+"/"+src;
    	new File(directorypath).mkdirs();
    	csvpath = directorypath + csvpath;
		
        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();

        PrintStream outstream = new PrintStream(new FileOutputStream(new File(csvpath)), true, "UTF-8");
        byte[] BOM = {(byte)0xEF, (byte)0xBB, (byte)0xBF};
        outstream.write(BOM);
        
        // Looping over all rows in the sheet
        for (int rowindex = 0, lastrowindex = sheet.getLastRowNum() ; rowindex <= lastrowindex ; rowindex++) {
        	Row row = sheet.getRow(rowindex);
        	if ( row == null ) {
        		outstream.println(',');
        		continue;
        	}
        	boolean firstCell = true;
        	for (int columnindex = 0, lastcolumnindex = row.getLastCellNum() ; columnindex < lastcolumnindex ; columnindex++) {
                Cell cell = row.getCell(columnindex, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                if ( ! firstCell ) outstream.print(',');
                if ( cell != null ) {
                	 String value = dataFormatter.formatCellValue(cell);
                	 outstream.print(value);
                }
                firstCell = false;
            }
        	outstream.println();
        }
		return outstream;
	}
}
