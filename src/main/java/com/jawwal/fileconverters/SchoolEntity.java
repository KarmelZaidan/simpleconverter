package com.jawwal.fileconverters;

import java.util.ArrayList;

public class SchoolEntity {

	
	private String schoolName;
	private String directorate;
	public ArrayList<String> branches;
	
	public SchoolEntity(String schoolname, String directorate) {
		this.setSchoolName(schoolname);
		this.setDirectorate(directorate);
		branches = new ArrayList<String>();
	}
	public SchoolEntity() {
		branches = new ArrayList<String>();
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getDirectorate() {
		return directorate;
	}
	public void setDirectorate(String directorate) {
		this.directorate = directorate;
	}
	public String getBranche(int index) {
		return this.branches.get(index);
	}
	public void setBranche(String branch) {
		this.branches.add(branch);
	}
}
