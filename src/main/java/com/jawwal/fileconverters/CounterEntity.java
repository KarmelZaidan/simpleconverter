package com.jawwal.fileconverters;

public class CounterEntity {

    private int schoolCounter;
    private int directorateCounter;
    private int brancheCounter;
    private int schoolbranch;
    private int studentCounter;

    public CounterEntity(){
        this.schoolCounter = 0;
        this.directorateCounter = 0;
        this.brancheCounter = 0;
        this.schoolbranch = 0;
        this.studentCounter = 1;
    }
    public CounterEntity(int schoolCounter, int directorateCounter, int brancheCounter, int schoolbranch, int studentCounter) {
        this.schoolCounter = schoolCounter;
        this.directorateCounter = directorateCounter;
        this.brancheCounter = brancheCounter;
        this.schoolbranch = schoolbranch;
        this.studentCounter = studentCounter;
    }

    public int getSchoolCounter() {
        return schoolCounter;
    }

    public void setSchoolCounter(int schoolCounter) {
        this.schoolCounter = schoolCounter;
    }

    public int getDirectorateCounter() {
        return directorateCounter;
    }

    public void setDirectorateCounter(int directorateCounter) {
        this.directorateCounter = directorateCounter;
    }

    public int getBrancheCounter() {
        return brancheCounter;
    }

    public void setBrancheCounter(int brancheCounter) {
        this.brancheCounter = brancheCounter;
    }

    public int getSchoolbranch() {
        return schoolbranch;
    }

    public void setSchoolbranch(int schoolbranch) {
        this.schoolbranch = schoolbranch;
    }

    public int getStudentCounter() {
        return studentCounter;
    }

    public void setStudentCounter(int studentCounter) {
        this.studentCounter = studentCounter;
    }
}
