package com.jawwal.fileconverters;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class ExcelToJSON {

	public int generalCounter = 0;
	private static CounterEntity counters;

	public ExcelToJSON(Sheet sheet, String csvpath) {}
	public ExcelToJSON(int generalCounter, CounterEntity counters) {
		this.generalCounter = generalCounter;
		this.counters = counters;
	}
	public ExcelToJSON() {}
	public int getGeneralCounter() {
		return generalCounter;
	}

	public void setGeneralCounter(int generalCounter) {
		this.generalCounter = generalCounter;
	}

	public static CounterEntity getCounters() {
		return counters;
	}

	public void setCounters(CounterEntity counters) {
		this.counters = counters;
	}

	public OutputStream convertExcelToJSON(Sheet sheet, String []header, Properties properties, String src, boolean isPass) throws IOException {
		
		// Define, Load and initialize variables
        DataFormatter dataFormatter = new DataFormatter();
        int wbheaderrownumber = new Integer(properties.getProperty("WB_HEADER_CELL_ROW_NUMBER")).intValue();
        int gazaheaderrownumber = new Integer(properties.getProperty("GAZA_HEADER_CELL_ROW_NUMBER")).intValue();
        int westbankresult= new Integer(properties.getProperty("NUMBER_OF_WB_ROWS_INCLUDE_HEADER")).intValue();
        int gazaresult= new Integer(properties.getProperty("NUMBER_OF_GAZA_ROWS_INCLUDE_HEADER")).intValue();
        int headerrowindex = (src.equals("WB"))? wbheaderrownumber : gazaheaderrownumber;
        new Integer(properties.getProperty("NUMBER_OF_COLUMNS")).intValue();
        String generalpath = properties.getProperty("GENERAL_PATH");
        ArrayList<SchoolEntity> schooldistricts = new ArrayList<SchoolEntity>();
        ArrayList<String> directorates = new ArrayList<String>();
        ArrayList<String> branches = new ArrayList<String>();
        ArrayList<String> schoolbranches = new ArrayList<String>();
        Map<String, Integer> statisticalmap = new HashMap<String, Integer>();
        boolean iscorrectresult = false;
        int rowsnumber = 1;
        int Dindex = 0;
        int Sindex = 0;
        int Bindex = 0;
        int Seatingindex = 0;
        int sid = getCounters().getStudentCounter();
        
        //Looping over all headers
        for(int i=0; i < header.length; i++) {
        	if(header[i].equals(properties.getProperty("EXPECTED_DISTRICT")) ){
        		Dindex = i;
        	}
        	if(header[i].equals(properties.getProperty("EXPECTED_SCHOOL")) ){
        		Sindex = i;
        	}
        	if(header[i].equals(properties.getProperty("EXPECTED_SECTION")) ){
        		Bindex = i;
        	}
        	if(header[i].equals(properties.getProperty("EXPECTED_SEATING_NUMBER"))) {
        		Seatingindex = i;
        	}
        }
        
        //Initialize and Create Directories
		String baseURL = "/"+generalpath+"/"+src, directorypath = "/"+generalpath+"/"+src;
    	new File(directorypath).mkdirs();
        
        String jsonpath = null;
        int fileNumber = 0;
        if(src.equals("WB") && isPass) {
        	jsonpath = properties.getProperty("WB_JSON_FILE_PATH");
        	directorypath = directorypath + "/Pass/";
        }else if(src.equals("WB") && !isPass) {
        	jsonpath = properties.getProperty("IC_WB_JSON_FILE_PATH");
        	directorypath = directorypath + "/Incomplete/";
        }
        else if(src.equals("Gaza") && isPass){
        	jsonpath = properties.getProperty("G_JSON_FILE_PATH");
        	directorypath = directorypath + "/Pass/";
        }else if(src.equals("Gaza") && !isPass) {
        	jsonpath = properties.getProperty("IC_G_JSON_FILE_PATH");
        	directorypath = directorypath + "/Incomplete/";
        }
        new File(directorypath).mkdirs();
        jsonpath = directorypath + jsonpath;
        
        PrintStream outstream = new PrintStream(new FileOutputStream(new File(jsonpath+fileNumber+".JSON")), true, "UTF-8");
        outstream.println("[");
        
        for (int rowindex = headerrowindex, lastrowindex = sheet.getLastRowNum(), partitionCount = 0 ; rowindex <= lastrowindex ; rowindex++, partitionCount++) {
        	Row row = sheet.getRow(rowindex);
        	if ( row == null ) {
        		outstream.println();
        		continue;
        	}
        	if(partitionCount > new Integer(properties.getProperty("FILE_SIZE")).intValue()) {
        		fileNumber++;
        		outstream.println("{");
                outstream.println('"'+"apply_year"+'"'+":"+'"'+properties.getProperty("APPLY_YEAR")+'"'+",");
                generalCounter++;
                outstream.println('"'+"seating_number"+'"'+":"+'"'+generalCounter+'"');
                outstream.println("}");
                outstream.println("]");
        		outstream = new PrintStream(new FileOutputStream(new File(jsonpath+fileNumber+".JSON")), true, "UTF-8");
        		outstream.println("[");
        		partitionCount = 0;
        	}
        	outstream.println("{");
        	outstream.println('"'+"PID"+'"'+":"+'"'+"1"+'"'+",");
        	outstream.println('"'+"SID"+'"'+":"+'"'+sid+'"'+",");
        	outstream.println('"'+"apply_year"+'"'+":"+'"'+properties.getProperty("APPLY_YEAR")+'"'+",");
        	SchoolEntity entity = new SchoolEntity();
        	String schoolbranch = "";
        	for (int columnindex = 0, lastcolumnindex = row.getLastCellNum() ; columnindex < lastcolumnindex ; columnindex++) {
                Cell cell = row.getCell(columnindex, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                
                //In case there is an empty mark
                if(cell == null) {
                	if(columnindex+1 == Bindex) {
                		outstream.println('"'+"average"+'"'+":"+'"'+"ناجح"+'"'+",");
                	}
                }
                if ( cell != null ) {
                	 String value = dataFormatter.formatCellValue(cell);                	 
                	 value = value.replaceAll("\"", "'");
                	 if(columnindex == Seatingindex) {
                		 value = value.substring(2);
                	 }
                	 outstream.println('"'+header[columnindex]+'"'+":"+'"'+value+'"'+",");

                	 if(isPass) {
						 //Districts and Schools table
						 if (columnindex == Dindex) {
							 if (!directorates.contains(value)) {
								 directorates.add(value);
							 }
							 entity.setDirectorate(value);
						 } else if (columnindex == Sindex) {
							 entity.setSchoolName(value);
							 schoolbranch = schoolbranch + value;
						 } else if (columnindex == Bindex) {
							 if (!branches.contains(value)) {
								 branches.add(value);
							 }
							 schoolbranch = schoolbranch + value + "_";
							 if (statisticalmap.get(value) == null) {
								 statisticalmap.put(value, new Integer("1"));
							 } else {
								 int branchcounter = statisticalmap.get(value) + 1;
								 statisticalmap.put(value, new Integer(branchcounter));
							 }

						 }
					 }
                }
                if(isPass) {
					schooldistricts.add(entity);
					if (!schoolbranches.contains(schoolbranch)) {
						schoolbranches.add(schoolbranch);
					}
				}
            }
        	if(isPass) {
        		outstream.println('"'+"student_status"+'"'+":"+'"'+"ناجح"+'"'+",");
        	}else {
        		outstream.println('"'+"student_status"+'"'+":"+'"'+"غير مستكمل"+'"'+",");
        		outstream.println('"'+"average"+'"'+":"+'"'+"غير مستكمل"+'"'+",");
        	}
        	outstream.println('"'+"notes"+'"'+":"+'"'+" "+'"');
        	outstream.println("},");
        	rowsnumber = rowsnumber + 1;
        	sid++;
        }
        outstream.println("{");
        outstream.println('"'+"apply_year"+'"'+":"+'"'+properties.getProperty("APPLY_YEAR")+'"'+",");
        generalCounter++;
        outstream.println('"'+"seating_number"+'"'+":"+'"'+generalCounter+'"');
        outstream.println();
        outstream.println("}");
        outstream.println("]");
		getCounters().setStudentCounter(sid);

        if(isPass) {
			Map<String, String> map = removeDuplicates(schooldistricts);
			startTesting(rowsnumber, westbankresult, gazaresult, iscorrectresult, src);
			String districtpath = baseURL + "/" + src + "_" + "DISTRICTS.JSON";
			String schoolspath = baseURL + "/" + src + "_" + "SCHOOL_DISTRICTS.JSON";
			String branchespath = baseURL + "/" + src + "_" + "BRANCHES.JSON";
			String schoolbranchpath = baseURL + "/" + src + "_" + "SCHOOL_BRANCHES.JSON";
			exportFiles(districtpath, schoolspath, branchespath, schoolbranchpath, directorates, branches, schooldistricts, schoolbranches, map, src, properties);
		}
		return outstream;
	}
	
	public OutputStream convertExcelToJSON_Top(Sheet sheet, String []header, Properties properties, String src) throws IOException {
		
		// Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();
        int sindex = 0;
        
        //Read property file values
        String jsonpath = properties.getProperty("TOP_JSON_FILE_PATH");
        int headerindex = new Integer(properties.getProperty("HEADER_ROW_INDEX_TOP")).intValue();
        String generalpath = properties.getProperty("GENERAL_PATH");
        String sections[] = {properties.getProperty("SECTIONS_ORDER1"), properties.getProperty("SECTIONS_ORDER2"), properties.getProperty("SECTIONS_ORDER3"), properties.getProperty("SECTIONS_ORDER4"),
        					 properties.getProperty("SECTIONS_ORDER5"), properties.getProperty("SECTIONS_ORDER6"), properties.getProperty("SECTIONS_ORDER7"), properties.getProperty("SECTIONS_ORDER8")};
        

        //Initialize and Create Directories
        String directorypath = "/"+generalpath+"/"+src;
    	new File(directorypath).mkdirs();
    	jsonpath = directorypath + "/" +jsonpath;
    	
        //Setup output stream
        PrintStream outstream = new PrintStream(new FileOutputStream(new File(jsonpath+ ".JSON")), true, "UTF-8");
        outstream.println("[");


        //Looping over all rows
        for (int rowindex = headerindex+3,  lastrowindex = new Integer(properties.getProperty("TOP_LAST_ROW_NUMBER")).intValue() ; rowindex <= lastrowindex ; rowindex++) {
        	Row row = sheet.getRow(rowindex);
        	if ( row == null ) {
        		sindex++;
        		sindex = sindex%sections.length;
        		rowindex = rowindex + 3;
        	}
        	else {
	        	outstream.println("{");
	        	outstream.println('"'+"PID"+'"'+":"+'"'+properties.getProperty("APPLY_YEAR")+'"'+",");
	        	outstream.println('"'+"SID"+'"'+":"+'"'+ rowindex +'"' + ",");
	        	outstream.println('"'+"branch"+'"'+":"+'"'+sections[sindex]+'"'+ ",");
	        	for (int columnindex = 0, lastcolumnindex = row.getLastCellNum() ; columnindex < lastcolumnindex ; columnindex++) {
	                Cell cell = row.getCell(columnindex, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
	                if ( cell != null ) {
	                	String value = dataFormatter.formatCellValue(cell); 
	                	value = value.replaceAll("\"", "'");
	                	value = value.replaceAll("“", "'");
	                	value = value.replaceAll("”", "'");
	                	outstream.println('"'+header[columnindex]+'"'+":"+'"'+value+'"'+",");
	                }
	            }
	        	outstream.println('"'+"notes"+'"'+":"+'"'+" "+'"');
	        	outstream.println("},");
	        	
	        }
        }
        outstream.println("{");
    	outstream.println('"'+"PID"+'"'+":"+'"'+"2"+'"'+",");
    	outstream.println('"'+"SID"+'"'+":"+'"'+"-1"+'"'+",");
    	outstream.println('"'+"branch"+'"'+":"+'"'+""+'"'+",");
    	outstream.println('"'+"notes"+'"'+":"+'"'+" "+'"');
    	outstream.println("}");
        outstream.println("]");
		return outstream;
	}
	
	private static void startTesting(int rowsnumber, int westbankresult, int gazaresult, boolean iscorrectresult, String src) {
		System.out.println("Test name: Number Of Rows");
        if( src.equals("WB") && (rowsnumber == westbankresult)) {
        	iscorrectresult = true;
        	System.out.println("--------- TEST PASS ---------");
        }else if(src.equals("WB")){
        	iscorrectresult = false;
        	System.out.println("--------- TEST FAIL ---------" + " Actual: "+rowsnumber+" "+"Expected: "+westbankresult);
        }
        if(src.equals("Gaza") && (rowsnumber == gazaresult)) {
        	iscorrectresult = true;
        	System.out.println("--------- TEST PASS ---------");
        }else if(src.equals("Gaza")) {
        	iscorrectresult = false;
        	System.out.println("--------- TEST FAIL ---------" + " Actual: "+rowsnumber+" "+"Expected: "+gazaresult);
        }
        
        if(iscorrectresult) {
        	System.out.println(src + " CONVERSION PASS\n\n\n");
        }else {
        	System.out.println(src + " CONVERSION FAIL\n\n\n");
        }
	}
	
	private static Map<String, String> removeDuplicates(ArrayList<SchoolEntity> entities) {
		Map<String, String> map = new HashMap<String, String>();
		for(SchoolEntity obj: entities) {
			map.put(obj.getSchoolName(), obj.getDirectorate());
		}
		return map;
	}
	
	private static void exportFiles(String districtspath, String schoolspath, String branchespath, String schoolbranchpath, ArrayList<String> directorates, ArrayList<String> branches, ArrayList<SchoolEntity> schools, ArrayList<String> schoolbranches, Map<String,String> map, String src, Properties properties) throws UnsupportedEncodingException, FileNotFoundException {
		
		PrintStream directoratesoutstream = new PrintStream(new FileOutputStream(new File(districtspath)), true, "UTF-8");
        directoratesoutstream.println("[");
        int sid = getCounters().getDirectorateCounter();
        for(int i = 0 ; i< directorates.size(); i++) {
        	sid++;
        	directoratesoutstream.println("{");
        	directoratesoutstream.println('"'+"PID"+'"'+":"+'"'+"1"+'"'+",");
        	directoratesoutstream.println('"'+"SID"+'"'+":"+'"'+sid+'"' + ",");
        	directoratesoutstream.println('"'+"district"+'"'+":"+'"'+directorates.get(i)+'"');
        	directoratesoutstream.println("}");
        	if(i != directorates.size()-1) {
        		directoratesoutstream.println(",");
        	}
        }
        directoratesoutstream.println("]");
        getCounters().setDirectorateCounter(sid);
		sid = getCounters().getBrancheCounter();
        PrintStream branchesoutstream = new PrintStream(new FileOutputStream(new File(branchespath)), true, "UTF-8");
        branchesoutstream.println("[");
        for(int i = 0 ; i< branches.size(); i++) {
        	sid++;
        	branchesoutstream.println("{");
        	branchesoutstream.println('"'+"PID"+'"'+":"+'"'+"1"+'"'+",");
        	branchesoutstream.println('"'+"SID"+'"'+":"+'"'+sid+'"' + ",");
        	branchesoutstream.println('"'+"branch"+'"'+":"+'"'+branches.get(i)+'"');
        	branchesoutstream.println("}");
        	if(i != branches.size()-1) {
        		branchesoutstream.println(",");
        	}
        }
        branchesoutstream.println("]");
        getCounters().setBrancheCounter(sid);
		sid = getCounters().getSchoolCounter();
        PrintStream schoolsoutstream = new PrintStream(new FileOutputStream(new File(schoolspath)), true, "UTF-8");
        schoolsoutstream.println("[");
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
        	sid++;
        	Map.Entry pair = (Map.Entry)it.next();
        	schoolsoutstream.println("{");
        	schoolsoutstream.println('"'+"PID"+'"'+":"+'"'+"1"+'"'+",");
        	schoolsoutstream.println('"'+"SID"+'"'+":"+'"'+sid+'"' + ",");
        	schoolsoutstream.println('"'+"school"+'"'+":"+'"'+pair.getKey()+'"' + ",");
        	schoolsoutstream.println('"'+"district"+'"'+":"+'"'+pair.getValue()+'"');
        	schoolsoutstream.println("},");
        }
        schoolsoutstream.println("{");
        schoolsoutstream.println('"'+"PID"+'"'+":"+'"'+"2"+'"'+",");
    	schoolsoutstream.println('"'+"SID"+'"'+":"+'"'+sid+'"' + ",");
    	schoolsoutstream.println('"'+"school"+'"'+":"+'"'+"No School"+'"' + ",");
    	schoolsoutstream.println('"'+"district"+'"'+":"+'"'+"No District"+'"');
    	schoolsoutstream.println("}");
        schoolsoutstream.println("]");

        PrintStream schoolbranchsoutstream = new PrintStream(new FileOutputStream(new File(schoolbranchpath)), true, "UTF-8");
        schoolbranchsoutstream.println("[");
        SchoolEntity entity = null;
        getCounters().setSchoolCounter(sid);
        sid = getCounters().getSchoolbranch();
        for(int i = 0 ; i < schoolbranches.size() ; i++) {
        	entity = new SchoolEntity();
        	String schoolbranch = schoolbranches.get(i);
        	if(!schoolbranch.contains("_")) {
        		continue;
        	}
	        String[] entityarray = schoolbranch.split("_");
	        if(entityarray.length != 2) {
	        	continue;
	        }
	        String branch = entityarray[0];
	        String name = entityarray[1];
	        entity.setSchoolName(name);
	        for(int j = 0 ; j < schoolbranches.size() ; j++) {
	        	String subschoolbranch = schoolbranches.get(j);
	        	if(!subschoolbranch.contains("_")) {
	        		continue;
	        	}
	           	String subentityarray[] = subschoolbranch.split("_");
	           	if(subentityarray.length != 2) {
		        	continue;
		        }

	           	String subbranch = subentityarray[0];
	           	String subname = subentityarray[1];
	           	if(name.equals(subname)) {
	           		if(!entity.branches.contains(subbranch)) {
	           			entity.setBranche(subbranch);
	           		}
	           	}
	        }//inner loop
	        if(map.containsKey(name)) {
	        	sid++;
		        schoolbranchsoutstream.println("{");
		        schoolbranchsoutstream.println('"'+"apply_year"+'"'+":"+'"'+properties.getProperty("APPLY_YEAR")+'"'+",");
		        schoolbranchsoutstream.println('"'+"SID"+'"'+":"+'"'+sid+'"' + ",");
		        schoolbranchsoutstream.println('"'+"school"+'"'+":"+'"'+entity.getSchoolName()+'"' + ",");
		        int index = 0;
		        for(index = 0 ; index  < entity.branches.size() ; index ++) {
		        	schoolbranchsoutstream.println('"'+"branch"+index+'"' +":"+'"'+entity.branches.get(index)+'"' + ",");
		        }
		        if(index < 10) {
		        	while(index < 10) {
			        	schoolbranchsoutstream.println('"'+"branch"+index+'"' +":"+'"'+"null"+'"' + ",");
			        	index++;
			        }
		        }
		        schoolbranchsoutstream.println('"'+"notes"+'"'+":"+'"'+" "+'"');
		        schoolbranchsoutstream.println("},");
		        map.remove(name);
	        }
        }//outer loop
        
        schoolbranchsoutstream.println("{");
        schoolbranchsoutstream.println('"'+"apply_year"+'"'+":"+'"'+"-1"+'"'+",");
        schoolbranchsoutstream.println('"'+"SID"+'"'+":"+'"'+"500000"+'"' + ",");
        schoolbranchsoutstream.println('"'+"notes"+'"'+":"+'"'+" "+'"');
        schoolbranchsoutstream.println("}");
        schoolbranchsoutstream.println("]");
        getCounters().setSchoolbranch(sid);
	}

}
