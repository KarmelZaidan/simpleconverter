package com.jawwal.fileconverters;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

public class ExcelLoader {

    public static void main(String[] args) throws IOException, InvalidFormatException {
    	
    	// load args variables
    	//String path = args[0];
    	String path = "/home/karmelnaser/Reconess/Docs/Injaz/Tawjihi_Configuration/config.properties";
    	CounterEntity counters = new CounterEntity();
		Integer fileCounter = new Integer(100000000);

    	// Load property file
    	Properties properties = getPropertyFile(path);

    	//Read property file values
    	String WBpath = properties.getProperty("WB_EXCEL_FILE_PATH");
    	String Gpath = properties.getProperty("G_EXCEL_FILE_PATH");
    	String Toppath = properties.getProperty("TOP_EXCEL_FILE_PATH");
    	
    	//Start Conversion on both WB and GAZA sheets
    	System.out.println("			WestBank conversion starting...");
    	fileCounter = startConversion(WBpath,"WB", properties, fileCounter, counters);
    	System.out.println("			Gaza conversion starting...");
    	fileCounter = startConversion(Gpath,"Gaza", properties, fileCounter, counters);
    	System.out.println("			Top10 conversion starting...");
    	startConversion(Toppath,"Top", properties, fileCounter, counters);

    	//Conversion done
        System.out.println("Conversion Done");
    }
    
    public static Integer startConversion(String path, String src, Properties properties, Integer fileCounter, CounterEntity counters) throws EncryptedDocumentException, InvalidFormatException, IOException {
    	
    	//Read property file values
    	int wbheaderindex = new Integer(properties.getProperty("WB_HEADER_CELL_ROW_NUMBER")).intValue();
        int gazaheaderindex = new Integer(properties.getProperty("GAZA_HEADER_CELL_ROW_NUMBER")).intValue();
    	int headerrowNumberTop = new Integer(properties.getProperty("HEADER_ROW_INDEX_TOP")).intValue();
		int headerrowNumber = (src.equals("WB"))? wbheaderindex : gazaheaderindex;


    	// Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = WorkbookFactory.create(new File(path));
        
        // Creating FormulaEvaluator
        FormulaEvaluator formulaEveluater = null;
        formulaEveluater = workbook.getCreationHelper().createFormulaEvaluator();
		String []header = null;

        if(src.equals("Top")){
			Sheet sheet = workbook.getSheetAt(new Integer(properties.getProperty("TOP_SHEET_NUMBER")).intValue());
			ExcelToJSON E2JSONConverter = new ExcelToJSON(fileCounter.intValue(), counters);
			header = createTopHeaderArray(headerrowNumberTop + 2, properties);
			OutputStream outstreamT = E2JSONConverter.convertExcelToJSON_Top(sheet, header, properties, src);
		}else{
			for(int index = 0; index < 2; index++){

				int sheetNumber = 0;
				boolean isPass = false;
				if(index == 0){
					isPass = true;
					sheetNumber = new Integer(properties.getProperty("PASS_SHEET_NUMBER")).intValue();
				}else if(index == 1){
					isPass = false;
					sheetNumber = new Integer(properties.getProperty("INCOMPLETE_SHEET_NUMBER")).intValue();
				}

				Sheet sheet = workbook.getSheetAt(sheetNumber);
				sheet = changeHeaderNames(sheet, headerrowNumber - 1, properties, src);
				ExcelToJSON E2JSONConverter = new ExcelToJSON(fileCounter.intValue(), counters);
				if(src.equals("WB")){
					header =  createHeaderArray(headerrowNumber - 1, sheet);
					OutputStream outstreamJ = E2JSONConverter.convertExcelToJSON(sheet, header, properties, src, isPass);
				}else {
					header =  createHeaderArray(headerrowNumber - 1, sheet);
					OutputStream outstreamJ = E2JSONConverter.convertExcelToJSON(sheet, header, properties, src, isPass);
				}
				fileCounter = new Integer(E2JSONConverter.getGeneralCounter() + 1);
			}
		}

        // Closing the workbook
        workbook.close();
        return fileCounter;
    }
    
    public static Sheet changeHeaderNames(Sheet sheet, int rowIndex, Properties properties, String src) {
    	
    	//Read Property file values
    	String seatingnumber = properties.getProperty("SEATING_NUMBER");
    	String markssummation = properties.getProperty("MARKS_SUMMATION");
    	String average = properties.getProperty("AVERAGE");
		String averageName2 = properties.getProperty("AVERAGE_2");
		String averageName3 = properties.getProperty("AVERAGE_3");
    	String section = properties.getProperty("SECTION");
    	String department = properties.getProperty("DISTRICT");
    	String school = properties.getProperty("SCHOOL");
    	String status = properties.getProperty("STUDENT_STATUS");

    	
    	String seatingnumber_expected = properties.getProperty("EXPECTED_SEATING_NUMBER");
    	String markssummation_expected = properties.getProperty("EXPECTED_MARKS_SUMMATION");
    	String average_expected = properties.getProperty("EXPECTED_AVERAGE");
    	String section_expected = properties.getProperty("EXPECTED_SECTION");
    	String department_expected = properties.getProperty("EXPECTED_DISTRICT");
    	String school_expected = properties.getProperty("EXPECTED_SCHOOL");
    	String student_status_expected = properties.getProperty("EXPECTED_STUDENT_STATUS");
    	
    	
    	//Change Header Names
    	Row row = sheet.getRow(rowIndex);
    	DataFormatter dataFormatter = new DataFormatter();
    	for(Cell cell : row) {
    		String value = dataFormatter.formatCellValue(cell);
    		if(value.equals(seatingnumber)) {
    			cell.setCellValue(seatingnumber_expected);
    			
    		}else if(value.equals(markssummation)) {
    			
    			cell.setCellValue(markssummation_expected);
    			
    		}else if(value.equals(average)) {
    			
    			cell.setCellValue(average_expected);
    			
    		}else if(value.equals(section)) {
    			
    			cell.setCellValue(section_expected);
    			
    		}else if(value.equals(department)) {
    			
    			cell.setCellValue(department_expected);
    			
    		}else if(value.equals(school)) {
    			
    			cell.setCellValue(school_expected);
    			
    		}else if(value.equals(status)) {

    			cell.setCellValue(student_status_expected);

    		}else if(value.equals(averageName2)){

				cell.setCellValue(average_expected);

			}else if(value.equals(averageName3)){

				cell.setCellValue(average_expected);
			}
    	}
    	
		return sheet;
    	
    }
    
    public static String[] createHeaderArray(int rowIndex, Sheet sheet) {
    	
    	Row row = sheet.getRow(rowIndex);
    	DataFormatter dataFormatter = new DataFormatter();
    	ArrayList<String> headerlist = new ArrayList<String>();
    	for(Cell cell: row) {
    		String value = dataFormatter.formatCellValue(cell);
    		headerlist.add(value);
    	}
    	String header[] = Arrays.copyOf(headerlist.toArray(), headerlist.toArray().length, String[].class);
    	
		return header;
    	
    }
    public static String[] createTopHeaderArray(int rowIndex, Properties prop) {
    	String header[] = {prop.getProperty("EXPECTED_TOP_SQ_SEQUENCE"), prop.getProperty("EXPECTED_TOP_FULL_NAME"), prop.getProperty("EXPECTED_TOP_MARK"), prop.getProperty("EXPECTED_TOP_SCHOOL"), prop.getProperty("EXPECTED_TOP_DISTRICT"), prop.getProperty("EXPECTED_TOP_STUDENT_STATUS")};
    	return header;
    }

    public static Properties getPropertyFile(String path) {

    	Properties prop = new Properties();
    	InputStream input = null;

    	try {

    		input = new FileInputStream(path);

    		prop.load(new InputStreamReader(input, Charset.forName("UTF-8")));

    	} catch (IOException ex) {
    		ex.printStackTrace();
    	} finally {
    		if (input != null) {
    			try {
    				input.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	return prop;
    }
}